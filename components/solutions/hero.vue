<script lang="ts">
import Vue from 'vue';
import { UseLocalizedVideoSubtitles } from '~/mixins';
import { IsMobileMixin } from '../../mixins';
import Badge from '../common/badge.vue';
import SolutionsVideoModal from './video-modal.vue';

/**
 * @typedef {Object} HeroData - Data object expected as a prop.
 * @property {boolean} parallax - Flag indicating whether to enable parallax effect.
 * @property {string} title_variant - Variant for the title.
 * @property {string} mobile_title_variant - Variant for the title on mobile.
 * @property {string} header - Header text.
 * @property {boolean} gradient - Flag indicating whether to apply a gradient to the container.
 * @property {Array<string>} note - Array of strings representing note lines.
 * @property {boolean} note_animation_duration - Animation duration for the note.
 * @property {boolean} title_animation_duration - Animation duration for the title.
 * @property {boolean} body_note - Body note text.
 * @property {Object} primary_btn - Primary button details.
 * @property {Object} secondary_btn - Secondary button details.
 * @property {boolean} footnote - Footnote text.
 * @property {boolean} video - Video details.
 * @property {boolean} image - Image details.
 * @property {boolean} bordered - Flag indicating whether the image is bordered.
 * @property {boolean} rectangular - Flag indicating whether the image is rectangular.
 * @property {boolean} no_image_alt - Flag indicating whether to show alternative image.
 * @property {boolean} rounded - Flag indicating whether the image is rounded.
 * @property {boolean} hide_in_mobile - Flag indicating whether to hide the image on mobile. (IMPORTANT: This flag also styles the "no image" infinity icon)
 * @property {boolean} image_purple_background - Flag indicating whether the image has a purple background.
 * @property {string} video_url - URL of the video.
 * @property {boolean} is_video_thumbnail - Flag indicating whether the image is a video thumbnail. (IMPORTANT: this introduces the SolutionsVideoModal as opposed to embbeded video)
 * @property {string} image_url - URL of the image.
 * @property {string} alt - Alt text for the image.
 * @property {boolean} video_purple_background - Flag indicating whether the video has a purple background.
 */

export default Vue.extend({
  name: 'SolutionsHero',

  /**
   * @property {Object} components - Vue components used in the template.
   * @property {Object} components.SolutionsVideoModal - Video modal component.
   * @property {Object} components.Badge - Badge component.
   */
  components: {
    SolutionsVideoModal,
    Badge,
  },

  /**
   * @property {Array} mixins - Vue mixins used in the component.
   * @property {Object} mixins.IsMobileMixin - Mobile detection mixin.
   */
  mixins: [IsMobileMixin, UseLocalizedVideoSubtitles],

  /**
   * @property {Object} props - Props expected by the component.
   * @property {HeroData} props.data - Data object received from the parent component.
   */
  props: {
    data: {
      type: Object,
      required: true,
      default: null,
    },
  },

  /**
   * @property {Object} data - Data properties of the component.
   * @property {boolean} data.parallax - Flag for enabling parallax effect.
   * @property {boolean} data.showModal - Flag for displaying the video modal.
   */
  data() {
    return {
      parallax: this.data.parallax || false,
      showModal: false,
    };
  },

  /**
   * @property {Object} computed - Computed properties of the component.
   * @property {string} computed.titleVariant - Computed property for determining the title variant.
   */
  computed: {
    titleVariant() {
      if (!this.data.title_variant) {
        return 'heading1-bold';
      }

      return this.isMobile && this.data.mobile_title_variant
        ? this.data.mobile_title_variant
        : this.data.title_variant;
    },
  },

  /**
   * @method mounted - Lifecycle hook called after the component is mounted.
   * @description Initializes the parallax effect if enabled.
   */
  mounted() {
    if (this.parallax) {
      const observer = new IntersectionObserver(this.parallaxSection, {
        rootMargin: '0px',
        threshold: 0,
      });
      const children = Array.from(document.querySelectorAll('.container__row'));
      children.forEach((child) => observer.observe(child));
    }
  },

  /**
   * @method parallaxSection - Method for handling the parallax effect on section scroll.
   * @param {Array} entries - Array of IntersectionObserver entries.
   */
  methods: {
    parallaxSection(entries: any) {
      const headerHeight = (document as any)
        .querySelector('.nav-hoc')
        .getBoundingClientRect().height;
      entries.forEach((entry: any) => {
        const element = entry.target;
        const text = element.querySelector('.text-column');
        const image = element.querySelector('.image-column');
        if (entry.intersectionRatio > 0) {
          document.addEventListener('scroll', () => {
            const textDistanceFromTop =
              text.getBoundingClientRect().top - headerHeight;
            const imgDistanceToTop =
              image.getBoundingClientRect().top - headerHeight;
            text.style.transform = `translateY(${
              1 - textDistanceFromTop / 100
            }%)`;
            image.style.transform = `translateY(${imgDistanceToTop / 100}%)`;
          });
        }
      });
    },

    /**
     * @method openModal - Method to open the video modal.
     */
    openModal() {
      this.showModal = true;
    },
  },
});
</script>

<template>
  <section class="hero-wrapper">
    <div
      class="container slp-overflow-x-hidden"
      :class="{ gradient: data.gradient }"
    >
      <SlpRow class="container__row">
        <SlpColumn :cols="6" class="text-column">
          <div
            v-if="data.note"
            :class="{
              'hero-wrapper__animate-wrapper': data.note_animation_duration,
            }"
          >
            <div
              :class="{
                'hero-wrapper__animate-wrapper__content':
                  data?.note_animation_duration,
              }"
              :style="{ animationDuration: data?.note_animation_duration }"
            >
              <Badge
                v-if="data.badge"
                :badge="data.badge"
                :badge-class="data.badge_class"
              />
              <SlpTypography
                v-for="line in data.note"
                :key="line"
                tag="h1"
                variant="heading5"
                class="note"
              >
                {{ line }}</SlpTypography
              >
            </div>
          </div>
          <div
            :class="{
              'hero-wrapper__animate-wrapper hero-wrapper__animate-wrapper--gradient':
                data?.title_animation_duration,
            }"
          >
            <div
              class="text"
              :class="[
                { gradient: data.gradient, display: data.display_size },
                {
                  'hero-wrapper__animate-wrapper__content':
                    data?.title_animation_duration,
                },
              ]"
              :style="{ animationDuration: data?.title_animation_duration }"
            >
              <SlpTypography
                v-if="data.german_title"
                :tag="data.note ? 'h2' : 'h1'"
                :variant="'heading2-bold'"
                class="slp-mt-16 slp-mt-md-0 slp-mb-16 header-text"
                >{{ data.title }}</SlpTypography
              >
              <SlpTypography
                v-else
                :tag="data.note ? 'h2' : 'h1'"
                :variant="titleVariant"
                class="slp-mt-16 slp-mt-md-0 slp-mb-16 header-text"
                >{{ data.title }}</SlpTypography
              >
              <span
                v-if="data.subtitle"
                class="text__span"
                v-html="$md.render(data.subtitle)"
              />
              <SlpTypography
                v-if="data.body_note"
                variant="body2"
                tag="p"
                class="slp-mt-16"
                >{{ data.body_note }}</SlpTypography
              >
            </div>
          </div>
          <div class="buttons slp-mt-32">
            <div
              v-if="data.primary_btn"
              :class="{
                'hero-wrapper__animate-wrapper':
                  data?.primary_btn?.button_animation_duration,
              }"
            >
              <SlpButton
                variant="primary"
                class="slp-mb-8 button"
                :class="[
                  { gradient: data.gradient },
                  {
                    'hero-wrapper__animate-wrapper__content':
                      data?.primary_btn?.button_animation_duration,
                  },
                ]"
                :style="{
                  animationDuration:
                    data?.primary_btn?.button_animation_duration,
                }"
                :data-ga-name="
                  data.primary_btn.data_ga_name || 'start your free trial'
                "
                :data-ga-location="
                  data.primary_btn.data_ga_location || 'header'
                "
                :href="data.primary_btn.url || '/free-trial/'"
                >{{ data.primary_btn.text }}
                <SlpIcon
                  v-if="data.primary_btn.icon"
                  :name="data.primary_btn.icon.name"
                  :variant="data.primary_btn.icon.variant"
                  class="slp-ml-8"
                />
              </SlpButton>
            </div>
            <div
              v-if="data.primary_btn"
              :class="{
                'hero-wrapper__animate-wrapper':
                  data?.secondary_btn?.button_animation_duration,
              }"
            ></div>
            <SlpButton
              v-if="data.secondary_btn"
              :variant="data.secondary_btn.variant || 'secondary'"
              class="slp-mb-8 hero-wrapper__button"
              :class="{
                'hero-wrapper__animate-wrapper__content':
                  data?.secondary_btn?.button_animation_duration,
              }"
              :href="
                !data.secondary_btn.modal
                  ? data.secondary_btn.url || '/sales/'
                  : undefined
              "
              :data-ga-name="data.secondary_btn.data_ga_name || 'contact sales'"
              :data-ga-location="
                data.secondary_btn.data_ga_location || 'header'
              "
              :style="{
                animationDuration:
                  data?.secondary_btn?.button_animation_duration,
              }"
              @click.native="data.secondary_btn.modal && openModal()"
              >{{ data.secondary_btn.text || 'Contact Sales' }}
              <SlpIcon
                v-if="data.secondary_btn.icon"
                :name="data.secondary_btn.icon.name"
                :variant="data.secondary_btn.icon.variant"
                class="slp-ml-8"
            /></SlpButton>
          </div>
          <div v-if="data.footnote" class="footnote slp-mt-32">
            <SlpTypography variant="body3" tag="p">{{
              data.footnote
            }}</SlpTypography>
          </div>
        </SlpColumn>
        <SlpColumn
          v-if="data.video && !data.image?.is_video_thumbnail"
          :cols="6"
          class="image-column column"
        >
          <iframe
            class="slp-my-64 slp-mr-64 video"
            :src="createVideoSource(data.video.video_url)"
            width="560"
            height="315"
            :title="data.header + ' YouTube Video'"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </SlpColumn>
        <SlpColumn v-else-if="data.image" :cols="5" class="image-column column">
          <div
            class="image-container"
            :class="{
              'image-container--bordered': data.bordered || data.image.bordered,
              'image-container--rectangular': data.image.rectangular,
              'image-container--alt': data.image.no_image_alt,
              'image-container--rounded': data.image.rounded,
              'image-container--no-image-mobile':
                data.hide_in_mobile || data.image.hide_in_mobile, // Style is necessary for the 'Infinity cropped no image' icon
              'image-container--plain':
                data.image.image_purple_background == false,
            }"
          >
            <div
              v-if="data.image.is_video_thumbnail"
              class="video-container"
              @click="openModal()"
            >
              <SlpIcon
                v-show="data.image.is_video_thumbnail"
                class="play-icon"
                name="play-circle"
                variant="marketing"
                size="lg"
                slp-color="color-text-link-100"
              />
              <img
                id="video-container__image"
                class="image thumbnail-image"
                :class="{
                  'image--bordered': data.image.bordered || data.bordered,
                }"
                :src="data.image.image_url"
                :alt="data.image.alt"
              />
            </div>
            <img
              v-else
              id="image-container__image"
              class="image"
              :class="{
                'image--bordered': data.image.bordered || data.bordered,
                'image--rectangular': data.image.rectangular,
                'image--rounded': data.image.rounded,
                'image--alt': data.image.no_image_alt,
                'image--position-left': data.image.position == 'left',
                'image--dim': data.image.dim,
                'image--small': data.image.small,
              }"
              :src="data.image.image_url"
              :alt="data.image.alt"
              loading="lazy"
            />
          </div>
        </SlpColumn>
        <div
          v-if="
            data.video_purple_background !== false ||
            data.video?.video_purple_background !== false ||
            data.image?.image_purple_background !== false
          "
          class="container__purple-background"
          :class="{
            'container__purple-background--no-image-mobile':
              data.hide_in_mobile ||
              data.video?.hide_in_mobile ||
              data.image?.hide_in_mobile,
          }"
        />
      </SlpRow>
    </div>
    <SolutionsVideoModal
      v-show="
        showModal && data.image?.is_video_thumbnail && data.video?.video_url
      "
      light-background
      :visible="showModal"
      @close="showModal = false"
    >
      <template #body>
        <iframe
          :src="createVideoSource(data.video?.video_url)"
          title="Video player"
          allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
      </template>
    </SolutionsVideoModal>
  </section>
</template>

<style lang="scss" scoped>
@import 'assets/css/animations';

@media (min-width: $breakpoint-lg) {
  .buttons {
    display: flex;
    .button:first-child {
      margin-right: $spacing-8;
    }
  }
}

/* Variables */
.hero-wrapper {
  --margin: 48px;
}
@media (min-width: $breakpoint-lg) {
  .hero-wrapper {
    --margin: 96px;
  }
}

.hero-wrapper {
  overflow: hidden;
  padding: var(--margin) 0;
  margin-bottom: var(--margin);

  @media (max-width: $breakpoint-md) {
    margin-bottom: $spacing-16;
    padding-bottom: 0;
  }
  .slp-container & {
    overflow: visible;
  }
  &__animate-wrapper {
    overflow-y: hidden;
    overflow-y: clip;
    &--gradient {
      @media (min-width: $breakpoint-lg) {
        width: 70rem;
      }
    }

    &__content {
      animation-name: intro-reveal;
      animation-fill-mode: forwards;
      animation-delay: 0.5s;
      transform: translate3d(0, 150%, 0);
    }
  }

  &__button {
    @media (max-width: $breakpoint-md) {
      width: 100%;
      justify-content: center;
      background-color: $color-surface-50;
    }
  }

  @keyframes intro-reveal {
    from {
      transform: translate3d(0, 150%, 0);
    }
    to {
      transform: translate3d(0, 0%, 0);
    }
  }
  .note {
    font-weight: 600;
    margin-bottom: $spacing-16;
  }

  .video {
    width: 100%;

    @media (max-width: $breakpoint-md) {
      margin-bottom: 0;
      margin-top: $spacing-16;
    }
  }
}

.gradient {
  .note {
    color: $color-primary-300;
    font-weight: normal;
    margin-bottom: 0;
  }
  .button {
    border: 1px solid transparent;
    border-radius: 4px;
    background:
      linear-gradient(90deg, black 0%, black 100%) padding-box,
      linear-gradient(90deg, #ffeff1 0%, #cbb2ff 100%) border-box;
    transition: all 0.3s;
    &:hover {
      color: black;
      background:
        linear-gradient(90deg, white 0%, white 100%) padding-box,
        linear-gradient(90deg, #ffeff1 0%, #cbb2ff 100%) border-box;
    }
  }
  .text {
    background: linear-gradient(267.59deg, #cbb3ff 16.24%, #ffeff1 94.71%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    background-clip: text;
    text-fill-color: transparent;
  }

  @media (min-width: $breakpoint-lg) {
    .text {
      width: 70rem;
    }
  }

  @media (max-width: $breakpoint-lg) {
    .container__row {
      flex-wrap: nowrap;
    }
    .image {
      opacity: 0.75;
      position: absolute;
      top: 15%;
      height: 85%;
    }
    .note {
      font-size: 16px;
    }
    .text h2,
    .text h1 {
      font-size: 60px;
      line-height: 60px;
    }
    .buttons {
      a.button {
        width: auto;
      }
    }
  }
}

.container {
  overflow: visible;
  margin-left: auto;
  margin-right: auto;
  max-width: 1170px;
  word-wrap: break-word;

  @media (max-width: $breakpoint-xl) {
    padding: 0 $spacing-16 $spacing-8 $spacing-16;
  }

  &__row {
    display: flex;
    align-items: center;
    justify-content: space-between;
    overflow: visible;
    position: relative;

    .text {
      @media (min-width: $breakpoint-lg) {
        &.display .header-text {
          font-size: 120px;
          line-height: 120px;
        }
      }

      &__span {
        font-size: 22px;
        font-weight: 400;
        letter-spacing: -0.8px;
        line-height: 28px;

        @media (min-width: $breakpoint-sm) {
          font-size: 24px;
          line-height: 32px;
        }
      }
    }

    .buttons {
      @media (max-width: $breakpoint-md) {
        a {
          width: 100%;
          justify-content: center;
        }
      }
    }
  }

  .column {
    overflow: visible;
  }

  .text-column {
    z-index: 2;
    transition: transform 0.1s ease;
    @media (min-width: $breakpoint-lg) {
      padding-right: $spacing-32;
    }
  }

  .image-column {
    z-index: 1;
    transition: transform 0.1s ease;
    @media (max-width: $breakpoint-md) {
      width: 100%;
    }
  }

  /**
 * Styles for the image container in the SolutionsHero component.
 *
 * The image container is responsible for displaying images or videos within the hero section.
 * Different variations and styles are applied based on breakpoints and specific modifiers.
 */
  .image-container {
    display: flex;
    justify-content: right;
    aspect-ratio: 14/9;

    /**
   * Responsive styling for small screens (up to $breakpoint-md).
   */
    @media (max-width: $breakpoint-md) {
      background: transparent;
      padding: 0;
      display: flex;
      justify-content: center;
      transform: none;
      opacity: 1;
    }

    /**
   * Bordered Image Container Modifier:
   * - Adds a bordered style to the image container.
   * - Applies a rounded bottom-right corner.
   */
    &--bordered {
      margin-bottom: $spacing-64;
      overflow: hidden;
      border-bottom-right-radius: 160px;

      /**
     * Responsive styling for medium and larger screens (from $breakpoint-md).
     */
      @media (min-width: $breakpoint-md) {
        margin: $spacing-64 0 $spacing-64 $spacing-16;
      }

      /**
     * Responsive styling for small screens (up to $breakpoint-md).
     */
      @media (max-width: $breakpoint-md) {
        margin-top: 0;
      }
    }

    /**
   * Rounded Image Container Modifier:
   * - Applies a circular clip path to create a rounded image container.
   * - Adds an animation for a circular mask effect.
   */
    &--rounded {
      clip-path: circle(0 at 50% 50%);
      animation: circular-mask 1s ease-in-out 0.2s forwards;

      /**
     * Responsive styling for medium and larger screens (from $breakpoint-md).
     */
      @media (min-width: $breakpoint-md) {
        margin: $spacing-64 0;
      }
    }

    /**
   * Shared styles for both rounded and bordered image containers.
   */
    &--rounded,
    &--bordered {
      width: 90vw;
      max-width: 90vw;
      max-height: 90vw;
      transform: translateX(0) translateY(0) !important;

      /**
     * Responsive styling for small screens (up to $breakpoint-sm).
     */
      @media (min-width: $breakpoint-sm) {
        width: 100%;
        max-width: 70vw;
        max-height: 70vw;
      }

      /**
     * Responsive styling for medium screens (from $breakpoint-md).
     */
      @media (min-width: $breakpoint-md) {
        width: 35vw;
        max-width: 594px;
        max-height: 456px;
      }

      /**
     * Responsive styling for large screens (from $breakpoint-lg).
     */
      @media (min-width: $breakpoint-lg) {
        margin: 0 0 0 $spacing-64;
      }
    }

    /**
   * Rectangular Image Container Modifier:
   * - Resets width and height to 'unset', allowing for the natural aspect ratio.
   */
    &--rectangular {
      width: unset;
      height: unset;
    }

    /**
   * Alternate Image Container Modifier:
   * - Adds extra margin for alternate styled image containers.
   * - Responsive padding for medium and larger screens (from $breakpoint-md).
   */
    &--alt {
      margin: $spacing-32 0;

      /**
     * Responsive styling for medium and larger screens (from $breakpoint-md).
     */
      @media (min-width: $breakpoint-md) {
        padding: $spacing-48 0;
      }
    }

    /**
   * Plain Image Container Modifier:
   * - Adds margin at the top for plain styled image containers.
   */
    &--plain {
      margin-top: $spacing-64;
    }

    /**
   * No Image on Mobile Modifier:
   * - Hides the image container on mobile screens.
   * - Adjusts the width for smaller screens.
   */
    &--no-image-mobile {
      @media (max-width: $breakpoint-lg) {
        display: none;
      }
      justify-content: left !important;
      align-items: center;
      margin: $spacing-16 0 $spacing-48 0;

      /* Responsive sizing for images or videos inside the container. */
      #image--container__image,
      #video--container__image {
        width: 130vw;

        /**
       * Responsive styling for medium screens (from $breakpoint-md).
       */
        @media (min-width: $breakpoint-md) {
          width: 69vw;
          min-width: 69vw;
        }

        /**
       * Responsive styling for large screens (from $breakpoint-lg).
       */
        @media (min-width: $breakpoint-lg) {
          width: 69vw;
          min-width: 69vw;
        }
      }
    }

    .video-container {
      cursor: pointer;
      position: relative;
      width: 100%;

      .play-icon {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 48px;
        color: black;
        cursor: pointer;
        z-index: 2;
      }
    }
  }

  .image {
    border-radius: 4px;
    object-fit: contain;

    &--dim {
      opacity: 0.15;
    }
    &--small {
      max-height: 530px;
    }
    &--bordered,
    &--rounded {
      height: 100%;
      transform: translate(0, 0);
    }
    &--position-left {
      object-position: left;
    }
    &--bordered {
      width: 100%;
      object-fit: cover;
    }

    &--rounded {
      width: 100%;
      object-fit: cover;
      object-position: -16px 0;
    }

    &--rectangular {
      border-bottom-right-radius: 0;
    }

    &__wrapper {
      width: 100%;
      height: 100%;
      overflow: hidden;
    }

    &--alt {
      width: 100%;
      max-width: 70vw;
      @media (min-width: $breakpoint-sm) {
        width: 100%;
      }
      @media (min-width: $breakpoint-md) {
        width: 50vw;
      }
    }
  }
  &__purple-background {
    position: absolute;
    z-index: -1;
    top: calc(0px - var(--margin));
    right: calc(300px - 40vw);
    background-color: $color-surface-800;
    height: calc(100% + var(--margin) * 2);
    width: 42vw;

    @media (max-width: $breakpoint-md) {
      transform: none;
      opacity: 1;
      bottom: 0;
      top: unset;
      left: -16px;
      height: 30%;
      width: 100vw;
    }
    &--no-image-mobile {
      @media (max-width: $breakpoint-lg) {
        display: none;
      }
    }
  }
  .aos-init,
  .aos-animate {
    @media (max-width: $breakpoint-md) {
      transform: none;
      opacity: 1;
    }
  }
}
</style>
