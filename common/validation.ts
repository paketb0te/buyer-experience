import { CtfEntry } from '../models';
interface Navigation {
  freeTrial: CtfEntry<any>;
  sales: CtfEntry<any>;
  login: CtfEntry<any>;
  navItems: CtfEntry<any>[];
  logo: CtfEntry<any>;
}

interface Footer {
  text: string;
  columns: CtfEntry<any>[];
  sourceCtaText: string;
  editCtaText: string;
  contributeCtaText: string;
}

export const validateNavigationResponse = (
  navigation: CtfEntry<Navigation>,
) => {
  const fields: Navigation = navigation.fields;

  if (fields.navItems.length < 1) {
    // eslint-disable-next-line
    console.error(`Check nav items array for missing links`);
    throw new Error();
  }

  return navigation;
};

export const validateFooterResponse = (footer: CtfEntry<Footer>) => {
  const fields: Footer = footer.fields;

  if (fields.columns.length < 1) {
    console.error(`Check footer columns array for missing links`);
    throw new Error();
  }

  return footer;
};
