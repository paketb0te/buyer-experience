import { metaDataHelper } from '~/services/events/events.helpers';

interface TeamOpsData {
  title: any;
  og_title?: any;
  description?: any;
  twitter_description?: any;
  og_description?: any;
  eventHero?: any;
  spotlight?: any;
  features?: any;
  video_spotlight?: any;
  card_section?: any;
  join_us?: any;
  nextSteps?: any;
}

function buttonBuilder(buttonRaw) {
  return {
    text: buttonRaw.text,
    url: buttonRaw.externalUrl,
    href: buttonRaw.externalUrl,
    data_ga_name: buttonRaw.dataGaName,
    data_ga_location: buttonRaw.dataGaLocation,
    icon: { name: buttonRaw.iconName, variant: buttonRaw.iconVariant },
  };
}
function imageBuilder(imageRaw, customFields?) {
  return {
    url: imageRaw.file?.url,
    alt: imageRaw.title,
    bordered: customFields?.bordered,
    image_purple_background: customFields?.image_purple_background,
  };
}
function listBuilder(listRaw) {
  return { title: listRaw.title, items: listRaw.list, icon: listRaw.iconName };
}
function heroBuilder(heroRaw) {
  const heroImageProps = heroRaw.backgroundImage?.fields && {
    ...imageBuilder(heroRaw.backgroundImage.fields),
    ...heroRaw.customFields.image,
  };
  return {
    title: heroRaw.title,
    subtitle: heroRaw.subheader,
    button: { ...buttonBuilder(heroRaw.primaryCta.fields) },
    ...heroRaw.customFields,
    image: { ...heroImageProps },
  };
}
function spotlightBuilder(spotlightRaw) {
  return {
    title: spotlightRaw.header,
    subtitle: spotlightRaw.subheader,
    description: spotlightRaw.text,
    button: { ...buttonBuilder(spotlightRaw.cta.fields) },
    image: spotlightRaw.backgroundImage?.fields && {
      ...imageBuilder(spotlightRaw.backgroundImage.fields),
      ...spotlightRaw.image.customFields,
    },
    list: listBuilder(spotlightRaw.assets[0].fields),
    ...spotlightRaw.customFields,
  };
}
function featuresBuilder(spotlightRaw) {
  const itemsBuilder = spotlightRaw.card.map((item) => {
    return {
      icon: {
        name: item.fields.iconName,
        variant: 'marketing',
        alt: `${item.fields.iconName} Icon`,
      },
      header: item.fields.title,
      text: item.fields.description,
    };
  });
  return {
    title: spotlightRaw.header,
    image: spotlightRaw.image.fields.image.fields && {
      ...imageBuilder(spotlightRaw.image.fields.image.fields),
    },
    accordion: {
      is_accordion: false,
      items: itemsBuilder,
    },
    ...spotlightRaw.customFields,
  };
}
function videoSpotlight(spotlightRaw) {
  return {
    title: spotlightRaw.title,
    subtitle: spotlightRaw.subtitle,
    description: spotlightRaw.description,
    button: { ...buttonBuilder(spotlightRaw.button.fields) },
    video: {
      url: spotlightRaw.video.fields.url,
      alt: spotlightRaw.video.fields.title,
    },
    ...spotlightRaw.customFields,
  };
}
function cardsSection(cardsRaw) {
  const cardsBuilder = cardsRaw.card.map((item) => {
    return {
      icon: {
        name: item.fields.iconName,
        slp_color: 'surface-700',
        variant: 'marketing',
        alt: `${item.fields.iconName} Icon`,
      },
      title: item.fields.title,
      description: item.fields.description,
      ...item.fields.customFields,
      link: {
        text: item.fields.button.fields.text,
        url: item.fields.cardLink,
      },
    };
  });
  return {
    title: cardsRaw.header,
    subtitle: cardsRaw.description,
    cards: cardsBuilder,
    ...cardsRaw.customFields,
  };
}
function joinUsBuilder(joinUsRaw) {
  return {
    title: joinUsRaw.title,
    description: joinUsRaw.description,
    button: { ...buttonBuilder(joinUsRaw.button.fields) },
    ...joinUsRaw.customFields,
  };
}
function pageCleanUp(page) {
  let nextSteps;
  const pageData = page.map((section) => {
    const id =
      section.fields?.componentName ||
      section.fields?.componentId ||
      section.sys?.contentType?.sys?.id;
    switch (id) {
      case 'eventHero':
        return {
          hero: { ...heroBuilder(section.fields) },
        };
      case 'spotlight':
        return {
          [id]: { ...spotlightBuilder(section.fields) },
        };

      case 'features':
        return {
          [id]: { ...featuresBuilder(section.fields) },
        };
      case 'video_spotlight':
        return {
          [id]: { ...videoSpotlight(section.fields) },
        };
      case 'card_section':
        return {
          [id]: { ...cardsSection(section.fields) },
        };
      case 'join_us':
        return {
          [id]: { ...joinUsBuilder(section.fields) },
        };
      case 'nextSteps':
        nextSteps = section.fields.variant || '1';
        return { name: null, data: null };
      default:
        return { name: null, data: null };
    }
  });

  const pageDataBuilder = pageData.reduce((previous: any, current: any) => {
    const mergedArray = {
      ...previous,
      ...current,
    };

    return mergedArray;
  });
  return { pageDataBuilder, nextSteps };
}

export function teamOpsDataHelper(data) {
  const { pageContent, seoMetadata } = data;

  const { pageDataBuilder, nextSteps } = pageCleanUp(pageContent);

  const page: TeamOpsData = {
    ...metaDataHelper(seoMetadata[0]),
    ...pageDataBuilder,
    nextSteps,
  };

  return page;
}
