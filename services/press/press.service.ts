import { CONTENT_TYPES } from '~/common/content-types';
import { toKebabCase } from '@/lib/utils';
import { getClient } from '~/plugins/contentful.js';
interface PressData {
  title?: string;
  description?: string;
  hero?: any;
  navigation?: any;
  nextSteps?: any;
  components?: any;
}
function formatDate(date): any {
  if (date) {
    const tempDate = new Date(`${date}T00:00:00`);
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return tempDate.toLocaleDateString('en-US', options);
  }
}
function pressCardBuilder(item) {
  return {
    title: item.title && item.title,
    subtitle: item.subtitle && item.subtitle,
    button: item.button && item.button.fields,
    image: item.image &&
      item?.image?.fields && {
        src:
          (item?.image?.fields?.file?.url && item?.image?.fields?.file?.url) ||
          '',
        alt: item?.image?.fields?.title && item?.image?.fields?.title,
      },
    cardLink: item.cardLink && item.cardLink,
    cardLinkDataGaName: item.cardLinkDataGaName && item.cardLinkDataGaName,
    cardLinkDataGaLocation:
      item.cardLinkDataGaLocation && item.cardLinkDataGaLocation,
  };
}
function sectionBuilder(section) {
  const items = section.card.map((item) => {
    if (item.sys.contentType.sys.id == 'card') {
      return pressCardBuilder(item.fields);
    }
  });

  return {
    name: 'press-section',
    header: section.header && section.header,
    id: section.componentName && section.componentName,
    description: section.description && section.description,
    image: section?.image?.fields && {
      src:
        (section?.image?.fields?.image?.fields?.file?.url &&
          section?.image?.fields?.image?.fields?.file?.url) ||
        '',
      alt: section?.image?.fields?.altText && section?.image?.fields?.altText,
    },
    cta: section?.cta?.fields && section?.cta?.fields,
    items,
  };
}
function menuBuilder(menu) {
  const items = menu.hyperlinks.map((link) => {
    return link.fields;
  });
  return { navigation: items };
}
async function getCardsFromcontentful(slug) {
  return await getClient()
    .getEntries({
      content_type: CONTENT_TYPES.CUSTOM_PAGE,
      'fields.slug[match]': slug,
      'fields.slug[ne]': slug,
      order: '-fields.date',
      select: 'fields.header, fields.description, fields.slug, fields.date',
      include: 3,
      limit: 5,
    })
    .then((query: any) => {
      const entryFields = query.items;
      return entryFields;
    })
    .catch((e) => {
      throw new Error(e);
    });
}
async function getPageCards(pageFields) {
  let cards = [];
  const slug = pageFields.slug;
  const landingGrids = pageFields.components?.filter((component) => {
    return component.sys.contentType.sys.id == 'landingGrid';
  });
  if (landingGrids[0].fields?.cards?.length) {
    cards = landingGrids[0].fields?.cards.sort(function (a, b) {
      if (!a.fields.date || !b.fields.date) {
        return 0;
      }
      return a.fields.date < b.fields.date
        ? 1
        : a.fields.date > b.fields.date
          ? -1
          : 0;
    });
    cards = cards.slice(0, 5);
  } else {
    cards = await getCardsFromcontentful(slug);
  }
  if (cards.length) {
    cards = cards.map((card) => {
      return {
        title: card.fields?.header && card.fields?.header,
        subtitle: card.fields?.date && formatDate(card.fields?.date),
        cardLink: card.fields?.slug,
        cardLinkDataGaName: card.fields?.header && card.fields?.header,
        cardLinkDataGaLocation: 'body',
      };
    });
  }

  return cards;
}
async function sectionByPageBuilder(page) {
  const fields = page.fields;

  const cards = await getPageCards(fields);
  return {
    name: 'press-section',
    header: fields.header && fields.header,
    id: fields.header && toKebabCase(fields.header),
    items: cards && cards,
  };
}
async function pageBuilder(page) {
  let result: PressData = {};
  result.components = [];

  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.CARD_GROUP:
        if (section.fields) {
          result.components.push(sectionBuilder(section.fields));
        }
        break;
      case CONTENT_TYPES.HERO:
        result = { ...result, hero: { ...section.fields } };
        break;
      case CONTENT_TYPES.SIDE_MENU:
        result = { ...result, ...menuBuilder(section.fields) };
        break;
      case CONTENT_TYPES.BUTTON:
        if (section?.fields) {
          result.components.push({ name: 'press-button', ...section.fields });
        }
        break;
      case CONTENT_TYPES.CUSTOM_PAGE:
        result.components.push(await sectionByPageBuilder(section));
        break;
      case CONTENT_TYPES.NEXT_STEPS:
        result.nextSteps = section.fields?.variant;
        break;
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    noIndex: seo.noIndex,
  };
}

export async function pressKitHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = await pageBuilder(pageContent);

  const metadata = metadataHelper(seoMetadata);
  const transformedData: PressData = {
    ...metadata,
    ...cleanPagecontent,
  };

  return transformedData;
}
