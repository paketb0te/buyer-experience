import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfAnchorLink,
  CtfButton,
  CtfCardGroup,
  CtfEntry,
  CtfHero,
  CtfSideNavigation,
} from '~/models';
import { convertToCaseSensitiveLocale } from '~/common/util';

export class InstallService {
  private readonly $ctx: Context;

  private readonly SECTION_IDS = {
    LINUX: 'official-linux-package',
    KUBERNETES: 'kubernetes-deployments',
    SUPPORTED_CLOUD: 'supported-cloud',
    OFFICIAL_METHODS: 'other-official-methods',
    COMMUNITY_CONTRIBUTED: 'community-contributed',
    ALREADY_INSTALLED: 'already-installed',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getContent(slug: string) {
    return await this.getContentfulData(slug);
  }

  private async getContentfulData(slug: string) {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const securityEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 3,
      locale,
    });

    const [items] = securityEntry.items;
    return { items, install: this.transformLandingData(items) };
  }

  public transformLandingData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const components = this.getCtfComponents(pageContent);

    return {
      ...seoMetadata[0]?.fields,
      ...components,
    };
  }

  private getCtfComponents(pageContent: CtfEntry<any>[]) {
    let result = {};

    for (const component of pageContent) {
      const { id } = component.sys.contentType.sys;
      switch (id) {
        case CONTENT_TYPES.SIDE_MENU: {
          result.side_menu = this.mapSideNavigation(component.fields);
          break;
        }
        case CONTENT_TYPES.HERO: {
          result.header = this.mapHero(component.fields);
          break;
        }
        case CONTENT_TYPES.CARD_GROUP: {
          result = { ...result, ...this.mapCardGroup(component.fields) };
          break;
        }
      }
    }

    return result;
  }

  private mapCardGroup(ctfCardGroup: CtfCardGroup) {
    const result = {};

    switch (ctfCardGroup.customFields.id) {
      case this.SECTION_IDS.LINUX: {
        result.linux = this.mapCardSection(ctfCardGroup);
        break;
      }
      case this.SECTION_IDS.KUBERNETES: {
        result.kubernetes = this.mapCardSection(ctfCardGroup);
        break;
      }
      case this.SECTION_IDS.SUPPORTED_CLOUD: {
        result.supported_cloud = this.mapCardSection(ctfCardGroup);
        break;
      }
      case this.SECTION_IDS.COMMUNITY_CONTRIBUTED: {
        result.unofficial_methods = this.mapCardSection(ctfCardGroup);
        break;
      }
      case this.SECTION_IDS.OFFICIAL_METHODS: {
        result.official_methods = this.mapCardSection(ctfCardGroup);
        break;
      }
      case this.SECTION_IDS.ALREADY_INSTALLED: {
        result.updates = this.mapCardSection(ctfCardGroup);
        break;
      }
    }

    return result;
  }

  private mapSideNavigation(ctfSideMenu: CtfSideNavigation) {
    const anchors = ctfSideMenu.anchors.map(
      (anchor: CtfEntry<CtfAnchorLink>) => ({
        text: anchor.fields.linkText,
        href: anchor.fields.anchorLink,
        data_ga_name: anchor.fields.dataGaName,
        data_ga_location: anchor.fields.dataGaLocation,
      }),
    );

    const hyperlinks = ctfSideMenu.hyperlinks.map(
      (link: CtfEntry<CtfButton>) => ({
        text: link.fields.text,
        href: link.fields.externalUrl,
        data_ga_name: link.fields.dataGaName,
        data_ga_location: link.fields.dataGaLocation,
      }),
    );

    return {
      text: ctfSideMenu.header,
      anchors: {
        text: ctfSideMenu.anchorsText,
        data: anchors,
      },
      hyperlinks: {
        text: ctfSideMenu.hyperlinksText,
        data: hyperlinks,
      },
    };
  }

  private mapHero(ctfHero: CtfHero) {
    return {
      title: ctfHero.title,
      subtitle: ctfHero.subheader,
      text: ctfHero.description,
    };
  }

  private mapCardSection(ctfCardGroup: CtfCardGroup) {
    const cards = ctfCardGroup.card.map((card) => ({
      title: card.fields.title,
      subtext: card.fields.subtitle,
      link_text: card.fields.button?.fields.text,
      link_url: card.fields.button?.fields.externalUrl,
      data_ga_name: card.fields.button?.fields.dataGaName,
      data_ga_location: card.fields.button?.fields.dataGaLocation,
      icon: card.fields.iconName && {
        name: card.fields.iconName,
      },
      ...card.fields.customFields,
    }));

    return {
      title: ctfCardGroup.header,
      subtitle: ctfCardGroup.subheader,
      text: ctfCardGroup.description,
      cards,
      ...ctfCardGroup.customFields,
    };
  }
}
