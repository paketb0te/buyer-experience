export function mapGitlabDuo(items) {
  let [
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    // eslint-disable-next-line prefer-const
    benefits,
  ] = items[0].fields.pageContent;
  //  Format content from contentful
  // Hero content
  hero = {
    button: {
      href: hero?.fields?.primaryCta?.fields?.externalUrl,
      text: hero?.fields?.primaryCta?.fields?.text,
      dataGaName: hero?.fields?.primaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.primaryCta?.fields?.dataGaLocation,
      variant: 'secondary',
    },
    secondaryButton: hero?.fields?.secondaryCta?.fields?.text && {
      href: hero?.fields?.secondaryCta?.fields?.externalUrl,
      text: hero?.fields?.secondaryCta?.fields?.text,
      dataGaName: hero?.fields?.secondaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.secondaryCta?.fields?.dataGaLocation,
      variant: 'primary',
    },
    description: hero?.fields?.subheader,
    image: {
      alt: hero?.fields?.backgroundImage?.fields?.title,
      src: hero?.fields?.backgroundImage?.fields?.file?.url,
    },
    ...hero.fields.customFields,
  };

  // Journey cards content

  journey = {
    features: journey?.fields?.data?.features,
  };

  // Video

  video = {
    text: video?.fields?.title,
    source: video?.fields?.url,
  };

  // Feature cards

  featureCards = {
    title: featureCards?.fields?.header,
    cards: featureCards?.fields?.card?.map((card: any) => ({
      name: card?.fields?.title,
      description: card?.fields?.description,
      href: card?.fields?.cardLink,
      icon: card?.fields?.iconName,
      data_ga_location: card?.fields?.cardLinkDataGaLocation,
      data_ga_name: card?.fields?.cardLinkDataGaName,
    })),
  };

  // Principles

  principles = principles?.fields?.data;

  //  Resources

  resources = {
    data: {
      column_size: 4,
      title: resources?.fields?.header,
      cards: resources?.fields?.card?.map((card: any) => ({
        link_text: 'Read more',
        data_ga_location: 'body',
        event_type: 'Blog',
        data_ga_name: card?.fields?.title,
        header: card?.fields?.title,
        href: card?.fields?.cardLink,
        icon: {
          name: card?.fields?.iconName,
          variant: 'marketing',
          alt: 'blog icon',
        },
        image: card?.fields?.image?.fields?.file?.url,
      })),
    },
  };

  //  Report CTA

  reportCta = {
    layout: 'dark',
    title: reportCta?.fields?.header,
    reports: reportCta?.fields?.card?.map((report: any) => ({
      url: report?.fields?.button?.fields?.externalUrl,
      description: report?.fields?.description,
      data_ga_name: report?.fields?.cardLinkDataGaName,
      data_ga_location: report?.fields?.cardLinkDataGaLocation,
    })),
  };

  const metadata = {
    title: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    ogTitle: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    description: items[0]?.fields?.seoMetadata[0]?.fields?.description,
    ogDescription: items[0]?.fields?.seoMetadata[0]?.fields?.ogDescription,
    ogType: items[0]?.fields?.seoMetadata[0]?.fields?.ogType,
    ogUrl: items[0]?.fields?.seoMetadata[0]?.fields?.ogUrl,
    ogSiteName: items[0]?.fields?.seoMetadata[0]?.fields?.ogSiteName,
    ogImage: items[0]?.fields?.seoMetadata[0]?.fields?.ogImage,
  };
  return {
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    metadata,
    benefits,
  };
}

export function mapGitlabDuoEnglishContent(items) {
  let [hero] = items[0].fields.pageContent;

  hero = {
    button: {
      href: hero?.fields?.primaryCta?.fields?.externalUrl,
      text: hero?.fields?.primaryCta?.fields?.text,
      dataGaName: hero?.fields?.primaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.primaryCta?.fields?.dataGaLocation,
      variant: 'secondary',
    },
    secondaryButton: hero?.fields?.secondaryCta?.fields?.text && {
      href: hero?.fields?.secondaryCta?.fields?.externalUrl,
      text: hero?.fields?.secondaryCta?.fields?.text,
      dataGaName: hero?.fields?.secondaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.secondaryCta?.fields?.dataGaLocation,
      variant: 'primary',
    },
    description: hero?.fields?.description,
    image: {
      alt: '',
      src: 'https://images.ctfassets.net/xz1dnu24egyd/2POqAdE7x75qplvXOgPJqA/1aec09006ae6e1f5eaa59452cf0216ec/Meet-GitLab-Duo-Thumbnail.png',
    },
    video: {
      video_url:
        'https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479',
    },
    ...hero.fields.customFields,
  };

  const metadata = {
    title: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    ogTitle: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    description: items[0]?.fields?.seoMetadata[0]?.fields?.description,
    ogDescription: items[0]?.fields?.seoMetadata[0]?.fields?.ogDescription,
    ogType: items[0]?.fields?.seoMetadata[0]?.fields?.ogType,
    ogUrl: items[0]?.fields?.seoMetadata[0]?.fields?.ogUrl,
    ogSiteName: items[0]?.fields?.seoMetadata[0]?.fields?.ogSiteName,
    ogImage: items[0]?.fields?.seoMetadata[0]?.fields?.ogImage,
  };

  const pricing = [
    {
      variant: 'tier',
      featured: true,
      data: {
        featured: false,
        price: 19,
        price_info: 'per user/month,',
        description: 'billed annually',
        hide_on_mobile: true,
        background_gradient: false,
        features: {
          list: [
            {
              text: '__Features include:__\n\n__Organizational User Controls__\n\n- User permissions for AI capabilities\n\n__Code Suggestions__\n\n- Code generation\n- Code completion\n- Available in many popular IDEs and supports 15 programming languages\n\n__Chat__\n\n- Code explanation\n- Test generation\n- Code refactoring',
            },
          ],
        },
        pill: 'New',
        title: 'GitLab Duo',
        subtitle: 'Pro',
        cardDescription:
          'For developers that want to focus on innovation and deliver high-quality software.',
        card_note: 'Only available for Premium and Ultimate customers.',
        primaryButton: {
          text: 'Contact us',
          disabled: false,
          url: 'https://about.gitlab.com/solutions/gitlab-duo-pro/sales/',
          data_ga_name: 'contact us',
          data_ga_location: 'body',
        },
        secondaryButton: {
          text: 'Existing customer? Buy now',
          url: 'https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html ',
          data_ga_name: 'duo pro subscription',
          data_ga_location: 'body',
        },
      },
    },
    {
      variant: 'tier',
      featured: true,
      data: {
        featured: false,
        price: 39,
        price_info: 'per user/month,',
        description: 'billed annually',
        hide_on_mobile: true,
        background_gradient: true,
        features: {
          list: [
            {
              text: '__Everything from GitLab Duo Pro, plus:__\n\n__Summarization and Templating tools__\n\n- Discussion summary\n- Merge request summary\n- Code review summary\n- Merge request template population\n\n__Security and vulnerability tools__\n\n- Vulnerability explanation\n- Vulnerability resolution\n\n__Advanced Troubleshooting__\n\n- Root Cause Analysis\n\n__AI analytics__\n\n- Value stream forecasting\n- AI Impact and Productivity Reporting\n\n__Personalize GitLab Duo__\n\n- Self-Hosted Model Deployment*\n- Model Personalization*',
            },
          ],
        },
        pill: 'Coming soon',
        title: 'GitLab Duo',
        subtitle: 'Enterprise',
        cardDescription:
          'For organizations that want AI throughout the software development lifecycle',
        card_note:
          '*Planned. Additional terms and fees may apply\n\nOnly available for Ultimate customers.\n',
        primaryButton: {
          text: 'Coming soon',
          disabled: true,
        },
      },
    },
  ];

  const resources = {
    id: 'resources',
    align: 'left',
    grouped: true,
    column_size: 4,
    title: 'Learn more about GitLab Duo',
    header_cta_text: 'View all resources',
    header_cta_href: '/resources/',
    header_cta_ga_name: 'View all resources',
    header_cta_ga_location: 'body',
    cards: [
      {
        event_type: 'Video',
        header: 'Meet GitLab Duo',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/wS2NkIeyOYrkiqadM1Jkd/eeabed3c2d08fa50047c70989d70b14d/Meet_GitLab_Duo_Thumbnail.png',
        href: 'https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'meet gitlab duo',
      },
      {
        event_type: 'Video',
        header: 'Code Suggestions',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/7paG5yX4I2FErEKb3022EN/cd480897c76cd1c5dd2a51426c8e0013/Code_Suggestions_Thumbnail.png',
        href: 'https://player.vimeo.com/video/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479',
        data_ga_name: 'code suggestions',
      },
      {
        event_type: 'Video',
        header: 'Chat',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/4y29zj5spjIUh2DaGdxk5q/f7d972269fd3b16501ef4d3b013f6c09/Chat_Thumbnail.png',
        href: 'https://player.vimeo.com/video/927753737?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'chat',
      },
      {
        event_type: 'Video',
        header: 'Code Review Summary',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/kfMhe63t5l6jHIxmmlq5z/a68a12a38b7faf7b37c821c5e3204ec1/Code_Review_Summary_Thumbnail.png',
        href: 'https://player.vimeo.com/video/929891003?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'code review summary',
      },
      {
        event_type: 'Video',
        header: 'Discussion Summary',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/4gvRwDXZ3zDwKKtCHiYhy6/c26a91ddfa12817c2bcee4e6a991260f/Discussion_Summary_Thumbnail.jpg',
        href: 'https://player.vimeo.com/video/928501915?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'discussion summary',
      },
      {
        event_type: 'Video',
        header: 'Vulnerability Explanation',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/64mYwKfirCjkHiapVtZlN4/830c78fa1902ed0a61ffdc9da822145c/Vulnerability_Explanation_Thumbnail.jpg',
        href: 'https://player.vimeo.com/video/930066123?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'vulernability explanation',
      },
      {
        event_type: 'Video',
        header: 'Code Explanation',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/1N6clW3Mq7Fu40uIEt2iav/6b487d1916276934b3641a456211835c/Code_Explanation_Thumbnail.jpg',
        href: 'https://player.vimeo.com/video/930066090?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'code explanation',
      },
      {
        event_type: 'Video',
        header: 'Suggested Reviewers',
        link_text: 'Watch now',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/5L9AwAebsBO8eIQxSH96nz/a388c454b847bc134082a352bea32082/Suggested_Reviewers_Thumbnail.png',
        href: 'https://player.vimeo.com/video/930066108?badge=0&autopause=0&player_id=0&app_id=58479',
        data_ga_name: 'suggested reviewers',
      },
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'Understand and resolve vulnerabilities with AI-powered GitLab Duo',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/1bIUeH2qhLxfgPJIZbBzD9/c916adff20eab529510a0e4eea5d9f4d/GitLab-Duo-Blog-Image-1.png',
        href: '/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/',
        data_ga_name:
          'Understand and resolve vulnerabilities with AI-powered GitLab Duo',
      },
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'Measuring AI effectiveness beyond developer productivity metrics',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/2IjSEZ7qEEcaDf2PlvJKRp/bb97ac8129ca9404b74ea0c13b5ed437/GitLab-Duo-Blog-Image-2.png',
        href: '/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/',
        data_ga_name:
          'Measuring AI effectiveness beyond developer productivity metrics',
      },
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'New report on AI-assisted tools points to rising stakes for DevSecOps',
        image:
          'https://images.ctfassets.net/xz1dnu24egyd/bH5lKxYxiDkZ4LULySnmO/ab7437e32e77d6b2f39bd70184edf913/GitLab-Duo-Blog-Image-3.png',
        href: '/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/',
        data_ga_name:
          'New report on AI-assisted tools points to rising stakes for DevSecOps',
      },
    ],
  };

  return {
    hero,
    metadata,
    pricing,
    resources,
  };
}
