---
  title: GitLab - La plateforme DevOps
  description: Une plateforme DevOps complète, fournie sous la forme d'une application unique avec une interface utilisateur, un magasin de données unifié et une sécurité intégrée dans le cycle de vie DevOps.
  components:
    - name: 'solutions-hero'
      data:
        title: Plateforme DevOps
        subtitle: GitLab est une application unique dotée de toutes les fonctionnalités d'une plateforme DevSecOps, permettant aux organisations de livrer des logiciels plus rapidement, tout en renforçant la sécurité et la conformité, maximisant ainsi le retour sur le développement de logiciels.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        secondary_btn:
          text: Regarder une démo
          url: /demo/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: plateforme GitLab pour DevOps"
    - name: 'copy-media'
      data:
        block:
          - header: La plateforme DevOps
            id: the-dev-ops-platform
            text: |
              Les outils DevOps ne doivent pas créer plus de problèmes qu'ils n'en résolvent. À mesure que les initiatives DevOps mûrissent, les chaînes d'outils fragiles construites à partir de solutions ponctuelles se dégradent, augmentant les coûts, réduisant la visibilité et créant des frictions au lieu de créer de la valeur. Contrairement aux chaînes d'outils conçues en interne, une véritable plateforme DevOps permet aux équipes d'itérer plus rapidement et d'innover ensemble. L'objectif est d'éliminer la complexité et les risques en vous fournissant tout ce dont vous avez besoin pour fournir plus rapidement des logiciels de meilleure qualité et plus sûrs, avec moins de risques et à moindre coût.
            link_href: /demo/
            link_text: En savoir plus
            video:
              video_url: https://www.youtube.com/embed/-_CDU6NFw7U?enablesjsapi=1
    - name: 'benefits'
      data:
        cards_per_row: 3
        benefits:
          - title: Travailler plus efficacement
            description: |
              Identifiez les obstacles et résolvez-les immédiatement, à l'aide d'un seul outil.
            icon:
              name: gitlab-release
              alt: Icône de release GitLab
              variant: marketing
              hex_color: '#451D7E'
          - title: Livraison plus rapide de meilleurs logiciels
            description: |
              Concentrez-vous sur la création de valeur, et non sur le maintien des intégrations.
            icon:
              name: agile
              alt: Icône Agile
              variant: marketing
              hex_color: '#451D7E'
          - title: Réduction des risques et des coûts
            description: |
              Automatisez la sécurité et la conformité sans compromettre la vitesse ou les dépenses.
            icon:
              name: lock-cog
              alt: Icône de rouage au centre d'un cadenas
              variant: marketing
              hex_color: '#451D7E'
    - name: 'copy-media'
      data:
        block:
          - header: Avantages de la plateforme GitLab DevOps
            id: git-lab-dev-ops-platform-benefits
            text: |
              En tant qu'application unique pour l'ensemble du cycle de vie DevOps, GitLab est :
              * **Complet:** visualisez et optimisez l'ensemble de votre cycle de vie DevOps avec des analyses à l'échelle de la plateforme dans le même système que celui où vous travaillez.
              * **Transparent:** utilisez un ensemble commun d'outils à travers les équipes et les étapes du cycle de vie, sans dépendre de plugins ou d'API tiers qui peuvent perturber votre flux de travail.
              * **Sécurisé:** recherchez les vulnérabilités et les violations de conformité à chaque validation.
              * **Transparent et conforme:** capturez et mettez automatiquement en corrélation toutes les actions, de la planification aux modifications de code en passant par les approbations, afin de faciliter la traçabilité lors des audits ou des rétrospectives.
              * **Facile à adopter:** apprenez une seule expérience utilisateur, gérez un seul magasin de données et exécutez le tout sur l'infrastructure de votre choix.
            image:
              image_url: /nuxt-images/topics/devops-lifecycle.svg
              alt: ""
    - name: 'pull-quote'
      data:
        quote: "Nous apportons à l'entreprise une plateforme que nos ingénieurs veulent réellement utiliser, ce qui favorise l'adoption par plusieurs équipes et augmente la productivité sans avoir à « forcer » qui que ce soit à l'adopter. Cela contribue réellement à créer un écosystème dans lequel nos utilisateurs finaux nous aident activement à atteindre nos objectifs stratégiques : plus de releases, de meilleurs contrôles, de meilleurs logiciels."
        source: George Grant
        link_href: /customers/goldman-sachs/
        link_text: En savoir plus
        data_ga_name: george grant quote
        data_ga_location: body
        shadow: true
    - name: 'copy-media'
      data:
        block:
          - header: Ressources
            id: resources
            text: |
              * **[Gartner :](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html){data-ga-name="gartner" data-ga-location="body"}** Guide de marché pour les plateformes de livraison de flux de valeur DevOps
              * **[Glympse :](/customers/glympse/){data-ga-name="glympse" data-ga-location="body"}** Consolidation de 20 outils dans GitLab
              * **[BI Worldwide :](/customers/bi_worldwide/){data-ga-name="BI worldwide" data-ga-location="body"}** Tirer parti d'une plateforme unique pour sécuriser leur code
            video:
              video_url: https://www.youtube.com/embed/gzYTZhJlHoI?enablesjsapi=1

