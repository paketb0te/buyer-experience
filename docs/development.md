# Setting up your development environment


## Dependencies

1. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. Create a GitLab account (internal employees and contractors will have one created through onboarding)
3. Install [Node and NPM](https://nodejs.org/en/download)
4. Install [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#mac-stable)
5. Ensure that you have VS Code or a text editor/IDE of your choice installed
6. Set up a SSH key pair with your GitLab account. [Configure your SSH client to point to the directory where the](https://docs.gitlab.com/ee/user/ssh.html#configure-ssh-to-point-to-a-different-directory) private key is stored


## Buyer Experience repository setup
1. In your terminal, run `git clone git@gitlab.com:gitlab-com/marketing/digital-experience/buyer-experience.git`
2. Create your `.env` file.
3. The following Contentful tokens are required to be added to the `.env` file:

```
CTF_SPACE_ID
CTF_CDA_ACCESS_TOKEN
CTF_PREVIEW_ACCESS_TOKEN
CTF_NAV_SPACE_ID
CTF_NAV_CDA_ACCESS_TOKEN
CTF_NAV_PREVIEW_ACCESS_TOKEN
```

4. Run `yarn install` or simply run `yarn` 
5. Run `yarn dev` for local development. This renders the site via server-side rendering
6. For additional troubleshooting or testing, you can run `yarn generate && yarn start`. This renders the site via static site generation, which is how we deploy our site to production

If this is your first time contributing to this project, it is highly recommend to look through our `nuxt.config.js`, various Typescript config and constant files, helpful utilities located at `/lib` and `/mixins`, our `/plugin` folder, and our documentation files (`/docs`).

## Code standards

1. We use Prettier and ESLint to maintain code standards
2. We run a Husky pre-push hook that will check for linting warnings and errors. Any errors will block you from pushing your code.
3. It's a good habit to run `yarn lintfix` before you committing your code

## Useful VS Code Extensions to enhance your productivity

1. [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
3. [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow)







